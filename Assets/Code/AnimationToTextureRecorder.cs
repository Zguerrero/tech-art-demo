using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class AnimationToTextureRecorder : MonoBehaviour
{
    private enum EncodeType
    {
        png,
        exr
    }

    [System.Serializable]
    private struct TextureParams
    {
        public string name;
        public TextureFormat textureFormat;
        public EncodeType encodeType;
    }

    [SerializeField] private Transform m_animationRoot;
    [SerializeField] private int m_recordEveryNStep = 1;
    [SerializeField] private float m_recordDuration = 1.0f;
    [SerializeField] private string m_savePath = "/";
    [SerializeField] private TextureParams m_positionsTextureParams;
    [SerializeField] private TextureParams m_rotationsTextureParams;
    [SerializeField] private string m_bakedMeshSavePath;
    [SerializeField] private string m_bakedMeshAssetName;
    [SerializeField] private List<Transform> m_transforms;
    [SerializeField] private bool m_calculateBounds;
    [SerializeField] Vector4 m_boundsMin = new Vector4(-1.0f, -1.0f, -1.0f, -1.0f);
    [SerializeField] Vector4 m_boundsMax = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);  
    [SerializeField] private Material m_material;

    private int m_transformsCount;
    private int m_framesCount;
    private List<Vector4[]> m_positionsBuffer;
    private List<Vector4[]> m_rotationsBuffer;
    private Coroutine m_recordCoroutine;

    private void OnEnable()
    {
        StartRecording();
    }

    [ContextMenu("Initialize")]
    private void Initialize()
    {
        m_transforms = new List<Transform>();
        VFXUtilities.GetAllTransformsInChildren(m_animationRoot, m_transforms);
    }

    public void StartRecording()
    {
        Initialize();

        if(m_recordCoroutine != null)
            StopCoroutine(m_recordCoroutine);

        m_recordCoroutine = StartCoroutine(RecordCoroutine());
    }

    private IEnumerator RecordCoroutine()
    {
        Debug.Log("Recording");

        WaitForFixedUpdate wait = new WaitForFixedUpdate();
        m_framesCount = 0;
        m_transformsCount = m_transforms.Count;
        float timer = 0.0f;
        float timerStep = Time.fixedDeltaTime * m_recordEveryNStep;
        m_positionsBuffer = new List<Vector4[]>();
        m_rotationsBuffer = new List<Vector4[]>();

        Record();
        
        for(float f = 0.0f; f < m_recordDuration; f += Time.fixedDeltaTime)
        {
            timer += Time.fixedDeltaTime;

            if(timer >= timerStep)
            {
                timer = 0.0f;
                Record();
            }

            yield return wait;
        }

        if(m_calculateBounds == true)
            CalculateBounds();

        SetBoundsInMaterial();

        Color[] positionsColors = GetColors(m_positionsBuffer, m_boundsMin, m_boundsMax);
        Color[] rotationsColors = GetColors(m_rotationsBuffer, new Vector4(-1.0f, -1.0f, -1.0f, -1.0f), new Vector4(1.0f, 1.0f, 1.0f, 1.0f));

        SaveDatasToTexture(positionsColors, m_positionsTextureParams);
        SaveDatasToTexture(rotationsColors, m_rotationsTextureParams);
        BakeMesh();
    }

    private void Record()
    {
        Vector4[] positionsDatas = new Vector4[m_transformsCount];
        Vector4[] rotationsDatas = new Vector4[m_transformsCount];

        for(int i = 0; i < m_transformsCount; i++)
        {
            positionsDatas[i] = m_transforms[i].position;

            Quaternion rotation = m_transforms[i].rotation;
            Vector4 quaternionVector = new Vector4(rotation.x, rotation.y, rotation.z, rotation.w);
            rotationsDatas[i] = quaternionVector;
        }


        m_positionsBuffer.Add(positionsDatas);
        m_rotationsBuffer.Add(rotationsDatas);
        m_framesCount += 1;
    }

    private void CalculateBounds()
    {
        Vector4 min = Vector4.positiveInfinity;
        Vector4 max = Vector4.negativeInfinity;

        for(int i = 0; i < m_framesCount; i++)
        {
            for(int j = 0; j < m_transformsCount; j++)
            {
                Vector4 value = m_positionsBuffer[i][j];
                min = Vector4.Min(min, value);
                max = Vector4.Max(max, value);
            }
        }

        m_boundsMin = min;
        m_boundsMax = max;
    }

    private void SetBoundsInMaterial()
    {
        m_material.SetVector("_BoundsMin", m_boundsMin);
        m_material.SetVector("_BoundsMax", m_boundsMax);
    }

    private Color[] GetColors(List<Vector4[]> buffer, Vector4 boundsMin, Vector4 boundsMax)
    {
        Color[] colors = new Color[m_framesCount * m_transformsCount];

        int index = 0;

        for(int i = 0; i < m_framesCount; i++)
        {
            for(int j = 0; j < m_transformsCount; j++)
            {
                colors[index] = VFXUtilities.RemapTo01(boundsMin, boundsMax, buffer[i][j]);
                index += 1;
            }
        }

        return colors;
    }

    private void SaveDatasToTexture(Color[] colors, TextureParams textureParams)
    {
        int width = m_transformsCount;
        int height = m_framesCount;

        Texture2D savedTexture = new Texture2D(width, height, textureParams.textureFormat, true);
       
        savedTexture.SetPixels(0, 0, width, height, colors, 0);

        byte[] bytes;
        string extention;

        if(textureParams.encodeType == EncodeType.png)
        {
            bytes = savedTexture.EncodeToPNG();
            extention = ".png";
        }
        else
        {
            bytes = savedTexture.EncodeToEXR();
            extention = ".exr";
        }


        string absolutePath = VFXUtilities.GetSavePathAbsolute(m_savePath, textureParams.name, extention);
        string relativePath = VFXUtilities.GetSavePathRelative(m_savePath, textureParams.name, extention);

        File.WriteAllBytes(absolutePath, bytes);
        ImportTexture(relativePath);
        Debug.Log("Texture saved at path : " + relativePath);
    }

    private void ImportTexture(string path)
    {
        AssetDatabase.Refresh();
        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(path);
        importer.alphaIsTransparency = false;
        importer.sRGBTexture = false;
        importer.npotScale = TextureImporterNPOTScale.None;
        importer.mipmapEnabled = false;
        importer.textureCompression = TextureImporterCompression.Uncompressed;
        EditorUtility.SetDirty(importer);
        importer.SaveAndReimport();
    }

    private void BakeMesh()
    {
        CombineInstance[] combines = new CombineInstance[m_transformsCount];

        for(int i = 0; i < m_transformsCount; i++)
        {
            Mesh originalMesh = m_transforms[i].GetComponent<MeshFilter>().sharedMesh;
            
            Mesh mesh = new Mesh();
            mesh.vertices = originalMesh.vertices;
            mesh.triangles = originalMesh.triangles;
            mesh.uv = originalMesh.uv;
            mesh.normals = originalMesh.normals;
            mesh.tangents = originalMesh.tangents;

            Vector2[] uvs = mesh.uv;
            int uvsLenght = uvs.Length;

            Vector3[] newUVs = new Vector3[uvsLenght];

            float uvIndex = ((float)i + 0.5f) / (float)m_transformsCount;

            for(int j = 0; j < uvsLenght; j++)
            {
                newUVs[j].x = uvs[j].x;
                newUVs[j].y = uvs[j].y;
                newUVs[j].z = uvIndex;
            }

            mesh.SetUVs(0, newUVs);
            combines[i].mesh = mesh;
        }

        Mesh combinedMesh = new Mesh();
        combinedMesh.CombineMeshes(combines, true, false, false);
        combinedMesh.RecalculateNormals();
        combinedMesh.RecalculateTangents();
        combinedMesh.RecalculateBounds();

        Bounds bounds = combinedMesh.bounds;
        Vector3 min = combinedMesh.bounds.min;
        Vector3 max = combinedMesh.bounds.max;

        min.x += m_boundsMin.x;
        min.y += m_boundsMin.y;
        min.z += m_boundsMin.z;

        max.x += m_boundsMax.x;
        max.y += m_boundsMax.y;
        max.z += m_boundsMax.z;

        bounds.min = min;
        bounds.max = max;

        combinedMesh.bounds = bounds;

        string finalPath = VFXUtilities.GetSavePathRelative(m_bakedMeshSavePath, m_bakedMeshAssetName, ".mesh");
        Debug.Log("mesh saved at path : " + finalPath);

        AssetDatabase.CreateAsset(combinedMesh, finalPath);
        AssetDatabase.SaveAssets();
    }
}
