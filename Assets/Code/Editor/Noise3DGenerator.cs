using UnityEngine;
using UnityEditor;
using UnityEngine.Experimental.Rendering;

public class Noise3DGenerator : EditorWindow
{
    private struct NoiseParameters
    {
        public float multiplier;
        public float noiseMix;
        public Vector4 noiseSize;
        public float noiseLayerWeightFactor;
        public int noiseOctaves;
        public int noiseSeed;
        public Vector4 voronoiSize;
        public float voronoiLayerWeightFactor;
        public int voronoiOctaves;
        public int voronoiSeed;

        public void Initialize()
        {
            multiplier = 1.0f;
            noiseMix = 0.25f;
            noiseSize = new Vector4(1.0f, 1.0f, 1.0f, 4.0f);
            noiseLayerWeightFactor = 0.5f;
            noiseOctaves = 1;
            noiseSeed = 0;
            voronoiSize = new Vector4(1.0f, 1.0f, 1.0f, 8.0f);
            voronoiLayerWeightFactor = 0.5f;
            voronoiOctaves = 1;
            voronoiSeed = 0;
        }

        public static int GetStride()
        {
            return sizeof(float) * 12 + sizeof(int) * 4;
        }

        public void CopyFrom(NoiseParameters other)
        {
            this = other;
        }
    }

    [SerializeField] private ComputeShader m_computeShader;
    private int m_textureSizeX = 32;
    private int m_textureSizeY = 32;
    private int m_textureSizeZ = 32;
    private NoiseParameters[] m_noiseParameters;
    private string m_savePath = "/Art/Textures/";
    private string m_textureName = "3DTexture";

    private int m_kernelID;
    private int m_threadGroupsX;
    private int m_threadGroupsY;
    private int m_threadGroupsZ;
    private ComputeBuffer m_colorsBuffer;
    private ComputeBuffer m_noiseParametersBuffer;
    private Color[] m_colors;
    private static readonly int m_colorsBufferID = Shader.PropertyToID("_Colors");
    private static readonly int m_noiseParametersBufferID = Shader.PropertyToID("_NoiseParameters");
    private static readonly int m_textureSizeID = Shader.PropertyToID("_TextureSize");

    private Vector2 m_scrollPosition;
    private bool m_showBaseParameters;
    private bool[] m_showNoiseParameters = new bool[4];
    private static GUIStyle s_guiBox;

    private static string[] s_channelLabels = new string[]{"R CHANNEL", "G CHANNEL", "B CHANNEL", "A CHANNEL"};
    private static string s_baseParameters = "Base Parameters";
    private static string s_noiseParametersLabel = "Noise Parameters";
    private static string s_voronoiParametersLabel = "Voronoi Parameters";
    private static string s_copyParametersLabel = "Copy Parameters";
    private static string[] s_copyFromLabels = new string[]{"Copy From R", "Copy From G", "Copy From B", "Copy From A"};
    private static string[] s_copyToLabels = new string[]{"Copy To R", "Copy To G", "Copy To B", "Copy To A"};
    private static string s_copyToOthers = "Copy To Others";
    private static string s_multiplierLabel = "Multiplier";
    private static string s_noiseMixLabel = "Noise Mix";
    private static string s_sizeLabel = "Size";
    private static string s_layerWeightFactorLabel = "Layer Weight Factor";
    private static string s_octavesLabel = "Octaves";
    private static string s_seedLabel = "Seed";


    [MenuItem("Tools/Noise 3D Generator")]
    static void Init()
    {
        EditorWindow.GetWindow(typeof(Noise3DGenerator)).Show();
    }

    void OnEnable()
    {
        m_noiseParameters = new NoiseParameters[4];

        for(int i = 0; i < 4; i++)
        {
            m_noiseParameters[i].Initialize();
        }
    }

    private void OnGUI()
    {
        m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition);

        m_showBaseParameters = EditorGUILayout.Foldout(m_showBaseParameters, "BASE PARAMETERS");
        if(m_showBaseParameters == true)
        {
            GUILayout.BeginHorizontal("TEXTURE SIZE", GetGUIBox());
            m_textureSizeX = EditorGUILayout.IntField(m_textureSizeX);
            m_textureSizeY = EditorGUILayout.IntField(m_textureSizeY);
            m_textureSizeZ = EditorGUILayout.IntField(m_textureSizeZ);  
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical("TEXTURE SAVE", GetGUIBox());
            m_savePath = EditorGUILayout.TextField("Save Path", m_savePath);
            m_textureName = EditorGUILayout.TextField("Texture Name", m_textureName);

            if(GUILayout.Button("SAVE TEXTURE") == true)
            {
                SaveTexture();
            }

            GUILayout.EndVertical();
        }

        for(int i = 0; i < 4; i++)
        {
            m_showNoiseParameters[i] = EditorGUILayout.Foldout(m_showNoiseParameters[i], s_channelLabels[i]);

            if(m_showNoiseParameters[i] == true)
            {
                GUILayout.BeginVertical(GetGUIBox());
                DisplayNoiseParametersGUI(i);
                GUILayout.EndVertical();   
            }
        }

        EditorGUILayout.EndScrollView();
    }

    private void DisplayNoiseParametersGUI(int i)
    {
        GUILayout.Space(20);

        GUILayout.BeginVertical(s_baseParameters, GetGUIBox());
        m_noiseParameters[i].multiplier = EditorGUILayout.FloatField(s_multiplierLabel, m_noiseParameters[i].multiplier);
        m_noiseParameters[i].noiseMix = EditorGUILayout.Slider(s_noiseMixLabel, m_noiseParameters[i].noiseMix, 0.0f, 1.0f);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(s_noiseParametersLabel, GetGUIBox());
        m_noiseParameters[i].noiseSize = EditorGUILayout.Vector4Field(s_sizeLabel, m_noiseParameters[i].noiseSize);
        m_noiseParameters[i].noiseLayerWeightFactor = EditorGUILayout.Slider(s_layerWeightFactorLabel, m_noiseParameters[i].noiseLayerWeightFactor, 0.0f, 1.0f);
        m_noiseParameters[i].noiseOctaves = EditorGUILayout.IntField(s_octavesLabel, m_noiseParameters[i].noiseOctaves);
        m_noiseParameters[i].noiseSeed = EditorGUILayout.IntField(s_seedLabel, m_noiseParameters[i].noiseSeed);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(s_voronoiParametersLabel, GetGUIBox());
        m_noiseParameters[i].voronoiSize = EditorGUILayout.Vector4Field(s_sizeLabel, m_noiseParameters[i].voronoiSize);
        m_noiseParameters[i].voronoiLayerWeightFactor = EditorGUILayout.Slider(s_layerWeightFactorLabel, m_noiseParameters[i].voronoiLayerWeightFactor, 0.0f, 1.0f);
        m_noiseParameters[i].voronoiOctaves = EditorGUILayout.IntField(s_octavesLabel, m_noiseParameters[i].voronoiOctaves);
        m_noiseParameters[i].voronoiSeed = EditorGUILayout.IntField(s_seedLabel, m_noiseParameters[i].voronoiSeed);
        GUILayout.EndVertical();

        GUILayout.BeginVertical(s_copyParametersLabel, GetGUIBox());

        GUILayout.BeginHorizontal();
        for(int j = 0; j < 4; j++)
        {
            if(j == i)
                continue;

            if(GUILayout.Button(s_copyFromLabels[j]))
            {
                m_noiseParameters[i].CopyFrom(m_noiseParameters[j]);
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        for(int j = 0; j < 4; j++)
        {
            if(j == i)
                continue;

            if(GUILayout.Button(s_copyToLabels[j]))
            {
                m_noiseParameters[j].CopyFrom(m_noiseParameters[i]);
            }
        }
        GUILayout.EndHorizontal();

        if(GUILayout.Button(s_copyToOthers))
        {
            for(int j = 0; j < 4; j++)
            {
                if(j == i)
                    continue;

                m_noiseParameters[j].CopyFrom(m_noiseParameters[i]);
            }
        }
        
        GUILayout.EndVertical();
    }

    private static GUIStyle GetGUIBox()
    {
        if(s_guiBox == null)
        {
            s_guiBox = new GUIStyle(GUI.skin.window);
            s_guiBox.padding = new RectOffset(25, 25, 25, 25);
            s_guiBox.margin = new RectOffset(10, 10, 10, 10);
            s_guiBox.stretchHeight = false;
            s_guiBox.fontStyle = FontStyle.Bold;
        }

        return s_guiBox;
    }

    private void InitializeComputeShader()
    {
        m_kernelID = m_computeShader.FindKernel("Main");
        m_colorsBuffer = new ComputeBuffer(m_textureSizeX * m_textureSizeY * m_textureSizeZ, sizeof(float) * 4, ComputeBufferType.Structured);
        m_noiseParametersBuffer = new ComputeBuffer(4, NoiseParameters.GetStride(), ComputeBufferType.Structured);
        m_noiseParametersBuffer.SetData(m_noiseParameters);

        m_computeShader.SetBuffer(m_kernelID, m_colorsBufferID, m_colorsBuffer);
        m_computeShader.SetBuffer(m_kernelID, m_noiseParametersBufferID, m_noiseParametersBuffer);
        m_computeShader.SetVector(m_textureSizeID, new Vector3(m_textureSizeX, m_textureSizeY, m_textureSizeZ));

        m_computeShader.GetKernelThreadGroupSizes(m_kernelID, out uint threadGroupSizeX, out uint threadGroupSizeY, out uint threadGroupSizeZ);
        m_threadGroupsX = Mathf.CeilToInt((float)m_textureSizeX / threadGroupSizeX);
        m_threadGroupsY = Mathf.CeilToInt((float)m_textureSizeY / threadGroupSizeY);
        m_threadGroupsZ = Mathf.CeilToInt((float)m_textureSizeZ / threadGroupSizeZ);
    }

    private void DispatchComputeShader()
    {
        InitializeComputeShader();
        m_computeShader.Dispatch(m_kernelID, m_threadGroupsX, m_threadGroupsY, m_threadGroupsZ);

        m_colors = new Color[m_textureSizeX * m_textureSizeY * m_textureSizeZ];
        m_colorsBuffer.GetData(m_colors);

        m_colorsBuffer.Release();
        m_noiseParametersBuffer.Release();
    }

    private void SaveTexture()
    {
        DispatchComputeShader();

        Texture3D texture3D = new Texture3D(m_textureSizeX, m_textureSizeY, m_textureSizeZ, GraphicsFormat.R8G8B8A8_UNorm, TextureCreationFlags.None);

        texture3D.SetPixels(m_colors, 0);

        string relativePath = VFXUtilities.GetSavePathRelative(m_savePath, m_textureName, ".asset");

        AssetDatabase.CreateAsset(texture3D, relativePath);
        AssetDatabase.Refresh();
        AssetDatabase.ImportAsset(relativePath);
        Selection.activeObject = AssetDatabase.LoadAssetAtPath(relativePath, typeof(Object));
    }
}
