using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupPhysicAnimation : MonoBehaviour
{
    [SerializeField] private bool m_playAnimation;
    [SerializeField] private AnimationCurve m_animationCurve;
    [SerializeField] private float m_animationDuration;
    public List<Transform> m_transforms;
    public Rigidbody[] m_rigidBodies;
    [SerializeField] private float m_density;
    [HideInInspector][SerializeField] private int m_transformsCount;
    [HideInInspector][SerializeField] private Vector3[] m_startPositions;
    [HideInInspector][SerializeField] private Quaternion[] m_startRotations;
    private Coroutine m_playAnimationCoroutine;

    private void Update()
    {
        if(m_playAnimation == true)
        {
            m_playAnimation = false;

            if(m_playAnimationCoroutine != null)
                StopCoroutine(m_playAnimationCoroutine);

            ResetRigidBodies();
            m_playAnimationCoroutine = StartCoroutine(PlayAnimationCoroutine());
        }
    }

    private IEnumerator PlayAnimationCoroutine()
    {
        WaitForFixedUpdate wait = new WaitForFixedUpdate();

        for(float f = 0.0f; f < 1.0; f += Time.fixedDeltaTime / m_animationDuration)
        {
            float ev = m_animationCurve.Evaluate(f);

            for(int i = 0; i < m_transformsCount; i++)
            {
                Rigidbody body = m_rigidBodies[i];

                if(body.isKinematic == true)
                {
                    if(body.position.y > ev)
                    {
                        body.isKinematic = false;
                    }
                }
            }

            yield return wait;
        }
    }

    [ContextMenu("Initialize")]
    private void Initialize()
    {
        Cleanup();

        m_transforms = new List<Transform>();
        VFXUtilities.GetAllTransformsInChildren(transform, m_transforms);

        m_transformsCount = m_transforms.Count;
        m_rigidBodies = new Rigidbody[m_transformsCount];
        m_startPositions = new Vector3[m_transformsCount];
        m_startRotations = new Quaternion[m_transformsCount];

        for(int i = 0; i < m_transformsCount; i++)
        {
            GameObject go = m_transforms[i].gameObject;

            MeshFilter meshFilter = go.GetComponent<MeshFilter>();

            if(meshFilter == null)
                continue;

            MeshCollider meshCollider = go.AddComponent<MeshCollider>();
            meshCollider.sharedMesh = meshFilter.sharedMesh;
            meshCollider.convex = true;

            Rigidbody body = go.AddComponent<Rigidbody>();
            body.isKinematic = true;
            ComputeMass(body);
            m_rigidBodies[i] = body;
            m_startPositions[i] = m_transforms[i].position;
            m_startRotations[i] = m_transforms[i].rotation;
        }
    }

    [ContextMenu("Cleanup")]
    private void Cleanup()
    {
        if(m_transforms == null)
            return;

        for(int i = 0; i < m_transforms.Count; i++)
        {
            GameObject go = m_transforms[i].gameObject;
            GetAndDestroyComponent<Rigidbody>(go);
            GetAndDestroyComponent<Collider>(go);
        }

        m_rigidBodies = null;
    }

    [ContextMenu("Reset RigidBodies")]
    private void ResetRigidBodies()
    {
        for(int i = 0; i < m_transformsCount; i++)
        {
            Rigidbody body = m_rigidBodies[i];
            body.isKinematic = true;
            body.position = m_startPositions[i];
            body.rotation = m_startRotations[i];
        }
    }

    private void ComputeMass(Rigidbody rgdBody)
    {
        rgdBody.mass = GetApproximateVolume(rgdBody) * m_density;
    }

    private float GetApproximateVolume(Rigidbody rgdBody)
    {
        Renderer renderer = rgdBody.GetComponent<Renderer>();

        if(renderer == null)
            return 0.0f;

        Bounds bounds = renderer.bounds;
        return bounds.size.x * bounds.size.y * bounds.size.z;
    }

    private void GetAndDestroyComponent<T>(GameObject go) where T : Component
    {
        T comp = go.GetComponent<T>();

        if(comp != null)
        {
            DestroyImmediate(comp);
        } 
    }
}
