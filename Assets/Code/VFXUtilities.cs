using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public static class VFXUtilities
{
    public static void GetAllTransformsInChildren(Transform parentTransform, List<Transform> result)
    {
        foreach(Transform childTransform in parentTransform)
        {
            result.Add(childTransform);
            GetAllTransformsInChildren(childTransform, result);
        }
    }

    public static Vector4 RemapTo01(Vector4 min, Vector4 max, Vector4 value)
    {
        Vector4 a = value - min;
        Vector4 b = max - min;
        Vector4 result;
        result.x = Mathf.Clamp01(a.x / b.x);
        result.y = Mathf.Clamp01(a.y / b.y);
        result.z = Mathf.Clamp01(a.z / b.z);
        result.w = Mathf.Clamp01(a.w / b.w);

        return result;
    }

    public static string GetDirectory(string directoryPath)
    {
        string directory = Application.dataPath + directoryPath;

        if(!Directory.Exists(directory)) 
        {
            Directory.CreateDirectory(directory);
        }

        return directory;
    }

    public static string GetSavePathAbsolute(string directoryPath, string saveName, string extention)
    {
        string directory = GetDirectory(directoryPath);
        return directory + saveName + extention;
    }

    public static string GetSavePathRelative(string directoryPath, string saveName, string extention)
    {
        string directory = "Assets/" + directoryPath;

        if(!Directory.Exists(directory)) 
        {
            Directory.CreateDirectory(directory);
        }

        return directory + saveName + extention;     
    }
}
