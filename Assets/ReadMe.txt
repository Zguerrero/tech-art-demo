Voici une petite démo de tech Art, ce sont des tests que j'avais fais sur mon temps libre
(je préfère eviter de sortir le code d'Ankama). Tout ça est très expérimental, et aurais besoin
de polish pour être viable sur une prod (c'est vraiment pas user friendly aussi), 
mais je pense que c'est un bon aperçu de ce que je peux faire en tech art dans unity, que ça soit en C# ou en Shader.

il y a 3 éléments à voir dans cette Démo :
 

1 - Générateur de textures de Noise 3D :

L'outil est accessible via le menu Tools/Noise 3D Generator.
Il permet de créer des textures 3D, afin de les utiliser ensuite dans des VFXs par exemple.
Afin de générer la texture le plus rapidement possible, l'outil passe par un compute shader pour calculer
les valeurs de noise.

L'idée c'est de mélanger du Noise "Classique" (proche du Perlin Noise), avec du Voronoi.
On peu choisir quelle quantité de Noise/Voronoi on veux, et changer divers paramètres, tels que la taille
ou le nombre d'octaves.
Pour que la texture générée boucle, la taille du Noise doit impérativement être un multiple de la taille de la texture.

Aussi, les réglages sont séparés pour les canaux RGBA, afin de baker différents styles de noise dans la même texture.
Pour rendre le truc un peu plus facile d'utilisation, j'ai rajouté quelques boutons permettant de copier les paramètres
d'un canal à l'autre.


2 - Shader Volumétrique :

Dans la scène Démo, il y a une sphère avec un Shader volumétrique appliqué. C'était une expérimentation à base de
ray marching, assez couteux en perf. Il utilise une texture 3D générée avec l'outil du dessus.
Le shader est assez complexe, mais il permet de faire pleins d'effets différents.


3 - Le baking d'animation dans une texture :

L'idée est de baker les clés de positions et de rotations dans des textures et de jouer l'animation
avec vertex Shader. 
C'est particulièrement intéressant pour les animations de destruction, avec de la physique complexe. 
C'est beaucoup plus performant que de simuler la physique en temps réel.
En plus des textures, les meshes utilisés dans l'animation sont baké en un seul et unique mesh sur
lequel sera appliqué le shader qui permet de jouer l'animation.

Dans la scène Démo, il y a un GameObject AnimationToTextureRecorder avec un script portant le même nom.
C'est ce script qui s'occupe de baker le mesh et les textures, et de les sauvegarder dans le projet.
Il y a quelques réglages possibles et le baking se lance sur le OnEnable du script, donc le play mode
doit être lancé et le GameObject activé pour que ça marche. J'ai fais ça afin de ne pas avoir à simuler
la physique dans l'editeur.


