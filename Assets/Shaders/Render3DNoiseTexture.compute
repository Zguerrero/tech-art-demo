#pragma kernel Main
#include "VFX_Noises.hlsl"

struct NoiseParameters
{
    float multiplier;
    float noiseMix;
    float4 noiseSize;
    float noiseLayerWeightFactor;
    int noiseOctaves;
    int noiseSeed;
    float4 voronoiSize;
    float voronoiLayerWeightFactor;
    int voronoiOctaves;
    int voronoiSeed;
};

StructuredBuffer<NoiseParameters> _NoiseParameters;
RWStructuredBuffer<float4> _Colors;

float3 _TextureSize;

float ComputeNoise(float3 coords, NoiseParameters params)
{
    if(params.multiplier <= 0.0)
        return 0.0;

    float3 noiseSize = params.noiseSize.xyz * params.noiseSize.w;
    float3 voronoiSize = params.voronoiSize.xyz * params.voronoiSize.w;

    float3 noise = LayeredNoise3DLooping(coords, noiseSize, _TextureSize, params.noiseOctaves, params.noiseLayerWeightFactor, params.noiseSeed);
    float voronoi = LayeredVoronoi3DLooping(coords, voronoiSize, _TextureSize, params.voronoiOctaves, params.voronoiLayerWeightFactor, params.voronoiSeed);
    voronoi = 1.0 - saturate(voronoi);

    return lerp(voronoi, noise.x, params.noiseMix) * params.multiplier;
}

[numthreads(8,8,8)]
void Main (uint3 id : SV_DispatchThreadID)
{
    float3 coords = id;

    float4 color;
    color.x = ComputeNoise(coords, _NoiseParameters[0]);
    color.y = ComputeNoise(coords, _NoiseParameters[1]);
    color.z = ComputeNoise(coords, _NoiseParameters[2]);
    color.w = ComputeNoise(coords, _NoiseParameters[3]);

    _Colors[id.x + _TextureSize.x * id.y + _TextureSize.x * _TextureSize.y * id.z] = color;
}
