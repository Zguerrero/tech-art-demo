#ifndef VFX_NOISES_INCLUDED
#define VFX_NOISES_INCLUDED

#define FLOAT_MAX_VALUE 3.402823466e+38F;

float3 Hash33( float3 p, float3 loopSize )
{
    p = floor(frac(p / loopSize) * loopSize);
	p = float3( dot(p, float3(127.1,311.7, 74.7)),
			    dot(p, float3(269.5,183.3,246.1)),
			    dot(p, float3(113.5,271.9,124.6)));

	return frac(sin(p) * 43758.5453123);
}

float3 Noise3DLooping(float3 coords, float3 offset, float3 loopSize)
{
    coords += offset;
    float3 f = frac(coords);

    f = f * f * (3.0 - 2.0 * f);
    
    return lerp(lerp(lerp(Hash33(coords, loopSize), 
                          Hash33((coords + float3(1,0,0)), loopSize), f.x),
                     lerp(Hash33((coords + float3(0,1,0)), loopSize), 
                          Hash33((coords + float3(1,1,0)), loopSize), f.x), f.y),
                lerp(lerp(Hash33((coords + float3(0,0,1)), loopSize), 
                          Hash33((coords + float3(1,0,1)), loopSize), f.x),
                     lerp(Hash33((coords + float3(0,1,1)), loopSize), 
                          Hash33((coords + float3(1,1,1)), loopSize), f.x), f.y), f.z);
}

float3 LayeredNoise3DLooping(float3 coords, float3 size, float3 baseLoopSize, int octaves, float layerWeightFactor, float3 seed)
{   
    coords /= size;   
    float totalWeight = 1.0;
    float currentWeight = 1.0;
    float3 loopSize = baseLoopSize / size;
    float sizeMultiplier = 1.0;
    float3 offset = seed;
    
    float3 noise = Noise3DLooping(coords, offset, loopSize);

    for(int i = 0; i < octaves; i++)
    {   
        currentWeight *= layerWeightFactor;              
        totalWeight += currentWeight;
        loopSize *= 2.0;
        sizeMultiplier *= 2.0;
        offset += 0.777;
        
        noise += Noise3DLooping(coords*sizeMultiplier, offset, loopSize) * currentWeight;
    }
    
    noise /= totalWeight;

    return noise;
}

float Voronoi3DLooping(float3 coords, float3 offset, float3 loopSize)
{
    coords += offset;
    float3 f = frac(coords);

    float minDistance = FLOAT_MAX_VALUE;

    for(float z = -1.0; z <= 1.0f; z++)
    {
        for(float y = -1.0; y <= 1.0f; y++)
        {
            for(float x = -1.0; x <= 1.0f; x++)
            {
                float3 offset = float3(x, y, z);
                float3 randomPoint = Hash33(coords + offset, loopSize);
                float3 pointPosition = offset + randomPoint;
                float distance = length(f - pointPosition);

                if(distance < minDistance)
                {
                    minDistance = distance;
                }
            }
        }
    }

    return minDistance;
}

float LayeredVoronoi3DLooping(float3 coords, float3 size, float3 baseLoopSize, int octaves, float layerWeightFactor, float3 seed)
{      
    coords /= size;   
    float totalWeight = 1.0;
    float currentWeight = 1.0;
    float3 loopSize = baseLoopSize / size;
    float sizeMultiplier = 1.0;
    float3 offset = seed;
    
    float voronoi = Voronoi3DLooping(coords, offset, loopSize);

    for(int i = 0; i < octaves; i++)
    {   
        currentWeight *= layerWeightFactor;              
        totalWeight += currentWeight;
        loopSize *= 2.0;
        sizeMultiplier *= 2.0;
        offset += 0.777;
        
        voronoi += Voronoi3DLooping(coords*sizeMultiplier, offset, loopSize) * currentWeight;
    }
    
    voronoi /= totalWeight;

    return voronoi;
}
#endif