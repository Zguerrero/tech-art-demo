#ifndef VFX_UTILITIES_INCLUDED
#define VFX_UTILITIES_INCLUDED

    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"

    half remapIn01_half(half a, half b, half x)
    {
        return saturate((x - a)/(b - a));
    }

    half2 remapIn01_half2(half2 a, half2 b, half2 x)
    {
        return saturate((x - a)/(b - a));
    }

    half4 remapIn01_half4(half4 a, half4 b, half4 x)
    {
        return saturate((x - a)/(b - a));
    }

    float remapIn01_float(float a, float b, float x)
    {
        return saturate((x - a)/(b - a));
    }
    
    float2 remapIn01_float2(float2 a, float2 b, float2 x)
    {
        return saturate((x - a)/(b - a));
    }

    float4 remapIn01_float4(float4 a, float4 b, float4 x)
    {
        return saturate((x - a)/(b - a));
    }

    half remapOut01_half(half a, half b, half x)
    {
        return a + x * (b - a);
    }

    float3 remapOut01_float3(float3 a, float3 b, float3 x)
    {
        return a + x * (b - a);
    }

    //From "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Particles.hlsl"
    float SoftParticles(float far, float4 projection)
    {
        float fade = 1;
        if (far > 0.0)
        {
            float rawDepth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, UnityStereoTransformScreenSpaceTex(projection.xy / projection.w)).r;
            float sceneZ = (unity_OrthoParams.w == 0) ? LinearEyeDepth(rawDepth, _ZBufferParams) : LinearDepthToEyeDepth(rawDepth);
            float thisZ = LinearEyeDepth(projection.z / projection.w, _ZBufferParams);
            fade = saturate((sceneZ - thisZ) / far);
        }
        return fade;
    }

    //From "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Particles.hlsl"
    half CameraFade(float far, float4 projection)
    {
        float thisZ = LinearEyeDepth(projection.z / projection.w, _ZBufferParams);
        return half(saturate(thisZ / far));
    }

#endif