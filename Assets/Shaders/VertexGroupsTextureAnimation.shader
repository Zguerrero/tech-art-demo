Shader "VFX Test/Vertex Groups Texture Animation"
{
    Properties
    {
        _PositionsMap ("Positions Map", 2D) = "Grey" {}
        _RotationsMap ("Rotations Map", 2D) = "Grey" {}
        _Animation ("Animation", Range(0.01, 0.99)) = 0.0
        _BoundsMin ("Position Bounds Min", Vector) = (0.0, 0.0, 0.0)
        _BoundsMax ("Position Bounds Max", Vector) = (1.0, 1.0, 1.0)
        _Speed ("Speed", Float) = 0.0
    }

    SubShader
    {
        Pass
        {
            Tags { "Queue"="Geometry" "IgnoreProjector"="True" "RenderType"="Opaque" "PreviewType"="Plane" }

            Cull Back

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };

            sampler2D _PositionsMap;
            sampler2D _RotationsMap;
            float _Animation;
            float _Speed;
            float3 _BoundsMin;
            float3 _BoundsMax;

            float3 remapFrom01_float3(float3 a, float3 b, float3 x)
            {
                return a + x * (b - a);
            }

            float4 remapFrom01_float4(float4 a, float4 b, float4 x)
            {
                return a + x * (b - a);
            }

            //From https://www.geeks3d.com/20141201/how-to-rotate-a-vertex-by-a-quaternion-in-glsl/
            float3 RotateWithQuaternion(float3 position, float4 quaternion)
            { 
                 return position.xyz + 2.0 * cross(quaternion.xyz, cross(quaternion.xyz, position.xyz) + quaternion.w * position.xyz);
            }

            v2f vert (appdata v)
            {
                v2f o;

                float4 animationCoords = 0.0;
                animationCoords.x = v.uv.z;
                animationCoords.y = _Animation + _Time.y * _Speed;

                float3 positionOffsets = tex2Dlod(_PositionsMap, animationCoords).xyz;
                positionOffsets = remapFrom01_float3(_BoundsMin, _BoundsMax, positionOffsets);

                float4 rotations = tex2Dlod(_RotationsMap, animationCoords);
                rotations = rotations * 2.0 - 1.0;

                v.vertex.xyz = RotateWithQuaternion(v.vertex.xyz, rotations);
                v.vertex.xyz += positionOffsets;

                v.normal = RotateWithQuaternion(v.normal, rotations);

                float3 worldNormal = UnityObjectToWorldNormal(v.normal) * 0.5 + 0.5;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = fixed4(worldNormal.yyy, 1.0);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
    }
}
