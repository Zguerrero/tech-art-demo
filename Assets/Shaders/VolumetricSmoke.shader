Shader "VFX Test/Volumetric Smoke"
{
    Properties
    {
        _IterationsCount ("Iterations Count", int) = 32
        [HDR] _AlbedoColor1 ("Albedo Color 1", Color) = (1.0, 1.0, 1.0, 1.0)
        [HDR] _AlbedoColor2 ("Albedo Color 2", Color) = (0.5, 0.5, 0.5, 1.0)
        _ColorParams ("XY = Albedo Color Smoothstep; W = Light Sample Distance", Vector) = (0.0, 1.0, 1.0, 1.0)
        _Density ("Density", Float) = 0.1
        _Dissolve ("Dissolve", Range(0.0, 1.0)) = 1.0
        _ContactSoftness ("Contact Softness", Float) = 10.0
        _MaxDistance ("Max Distance", Float) = 10.0
        _DitheringFactor ("Dithering Factor", Float) = 1.0
        _DitherMap ("Dither Map", 2D) = "black" {}
        _MapParams( "Map Params", Vector) = (0.5, 0.5, 0.5, 0.5)
        _3DNoiseMap ("3D Noise Map", 3D) = "defaulttexture" {}
        _3DTextureSmoothstep("3D Texture Remap", Vector) = (0.0, 1.0, 0.5, 0.0)
        _3DTexture1Tiling("3D Texture 1 Tiling", Vector) = (1.0, 1.0, 1.0, 1.0)
        _3DTexture1Scrolling("3D Texture 1 Scrolling", Vector) = (0.0, 0.0, 0.0, 1.0)
        _3DTexture2Tiling("3D Texture 2 Tiling", Vector) = (1.0, 1.0, 1.0, 1.0)
        _3DTexture2Scrolling("3D Texture 2 Scrolling", Vector) = (0.0, 0.0, 0.0, 1.0)
        [KeywordEnum(Add, Mul, Min, Max, Lerp)] _NoiseMixMode ("Noise Mix Mode", Float) = 0
        _3DTexturesMixParams("X = Noise 1 Multiplier; Y = Noise 2 Multiplier; Z = Lerp Value", Vector) = (1.0, 1.0, 0.5, 0.0)
        _3DTexture2Distortion("3D Texture 2 Distortion", Float) = 0.1
        [KeywordEnum(Sphere, Plane)] _VolumeShape ("Volume Shape", Float) = 0
        [Toggle(_LOCALSPACEDISTANCEFIELD)] _LocalSpaceDistanceField ("Local Space Distance Field", Float) = 0
        [KeywordEnum(Geometry, Camera)] _RayOrigin ("Ray Origin", Float) = 0
        [KeywordEnum(MainDirectional, ParticleCustomData, None)] _LightingMode ("Lighting Mode", Float) = 0
        [KeywordEnum(Up, ObjectNormal, VolumeNormal, None)] _AmbientMode ("Ambient Mode", Float) = 0
        _VolumeNormalDistance ("Volume Normal Distance", Float) = 0.1
        [Toggle(_PARTICLESMODE_ON)] _ParticlesModeEnabled ("Enable Particles Mode", Float) = 0
        _ParticleParams ("Noise : X = Follow Particle, Y = Random Offset, Z = Particle Color Multiplier", Float) = (0.0, 0.0, 0.0, 0.0)

        [Space()]

        [Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 0
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("Z Test", Float) = 0
        _StencilRef ("Stencil Ref", int) = 0
        [Enum(UnityEngine.Rendering.CompareFunction)] _CompareFunction ("Stencil Compare Function", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)] _PassOperation ("Stencil Pass Operation", Float) = 0
        [Enum(UnityEngine.Rendering.StencilOp)] _FailOperation ("Stencil Fail Operation", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "RenderType" = "Transparent" 
            "IgnoreProjector" = "True" 
            "PreviewType" = "Plane" 
            "PerformanceChecks" = "False" 
            "RenderPipeline" = "UniversalPipeline" 
            "UniversalMaterialType" = "Lit"
        }

        Pass
        {
            Name "ForwardLit"
            Tags {"LightMode" = "UniversalForward"}

            Stencil
            {
                Ref [_StencilRef]
                Comp [_CompareFunction]
                Pass [_PassOperation]
                Fail [_FailOperation]
            } 

            Blend One OneMinusSrcAlpha
            ColorMask RGB
            Cull [_CullMode] 
            ZTest [_ZTest]
            ZWrite Off

            HLSLPROGRAM

            #pragma vertex Vertex
            #pragma fragment Fragment

            #include "VolumetricSmoke_Include.hlsl"

            #pragma shader_feature_local _PARTICLESMODE_ON
            #pragma shader_feature_local _VOLUMESHAPE_SPHERE _VOLUMESHAPE_PLANE
            #pragma shader_feature_local _LOCALSPACEDISTANCEFIELD
            #pragma shader_feature_local _NOISEMIXMODE_ADD _NOISEMIXMODE_MUL _NOISEMIXMODE_MIN _NOISEMIXMODE_MAX _NOISEMIXMODE_LERP
            #pragma shader_feature_local _RAYORIGIN_GEOMETRY _RAYORIGIN_CAMERA
            #pragma shader_feature_local _LIGHTINGMODE_MAINDIRECTIONAL _LIGHTINGMODE_PARTICLECUSTOMDATA _LIGHTINGMODE_NONE
            #pragma shader_feature_local _AMBIENTMODE_UP _AMBIENTMODE_OBJECTNORMAL _AMBIENTMODE_VOLUMENORMAL _AMBIENTMODE_NONE

            #pragma multi_compile_fog

            ENDHLSL
        }
    }
}
