#ifndef VOLUMETRICSMOKE_INCLUDE_INCLUDED
#define VOLUMETRICSMOKE_INCLUDE_INCLUDED

    #include "VFX_Utilities.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

    int _IterationsCount;
    float4 _AlbedoColor1;
    float4 _AlbedoColor2;
    float4 _ColorParams;
    float _Density;
    float _Dissolve;
    float _ContactSoftness;
    float _MaxDistance;
    float _DitheringFactor;
    float4 _MapParams;
    float4 _3DTextureSmoothstep;
    float4 _3DTexture1Tiling;
    float4 _3DTexture1Scrolling;
    float4 _3DTexture2Tiling;
    float4 _3DTexture2Scrolling;
    float4 _3DTexturesMixParams;
    float _3DTexture2Distortion;
    float _VolumeNormalDistance;
    float4 _ParticleParams;

    float4 _DitherMap_TexelSize;
    TEXTURE2D(_DitherMap); SAMPLER(sampler_DitherMap);
    TEXTURE3D(_3DNoiseMap); SAMPLER(sampler_3DNoiseMap);

    struct Attributes
    {
        float4 positionOS : POSITION;
        float3 normalOS : NORMAL;

        #ifdef _PARTICLESMODE_ON
            float4 particleColor : COLOR;
            float4 particleCustomData1 : TEXCOORD0;
            float4 particleCustomData2 : TEXCOORD1;
            float4 particlePosition : TEXCOORD2; // XYZ = particlePosition XYZ; Z = particleScale X
            float3 particleScale : TEXCOORD3; // XY = particleScale YZ; Z = Random
        #endif
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float4 positionWS : TEXCOORD0;
        float4 projectedPosition : TEXCOORD1;
        float3 normalWS : TEXCOORD2;

        #ifdef _PARTICLESMODE_ON
            float3 particlePosition : TEXCOORD4;
            float4 particleCustomData1 : TEXCOORD5;
            float4 particleCustomData2 : TEXCOORD6;
            float4 particleScale : TEXCOORD7;
            float4 particleColor : COLOR;
        #endif
    };

    struct ParticlesDatas
    {
        float3 position;
        float3 noiseOffset;
        float3 scale;
        float4 color;
    };

    struct RayMarchInput
    {
        float3 rayDirection;
        float3 origin;
        float maxDistance;
        float stepDistance;
        float startRayDistance;
        float3 lightDirection;
        float3 lightColor;
        ParticlesDatas particlesDatas;
    };

    struct RayMarchOutput
    {
        float4 albedo;
        float3 normal;
        float3 lightColor;
    };

    Varyings Vertex (Attributes input)
    {
        Varyings output;

        VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

        output.positionCS = vertexInput.positionCS;
        output.positionWS.xyz = vertexInput.positionWS;

        #ifdef _RAYORIGIN_GEOMETRY
            output.positionWS.w = vertexInput.positionVS.z;
        #else
            output.positionWS.w = 0.0;
        #endif

        output.projectedPosition = vertexInput.positionNDC;

        output.normalWS = TransformObjectToWorldNormal(input.normalOS);

        #ifdef _PARTICLESMODE_ON
            output.particlePosition = input.particlePosition.xyz;
            output.particleScale.xyz = float3(input.particlePosition.w, input.particleScale.x, input.particleScale.y);
            output.particleScale.w = input.particleScale.z * _ParticleParams.y;
            output.particleColor = input.particleColor;
            output.particleColor.rgb *= _ParticleParams.z;
            output.particleCustomData1 = input.particleCustomData1;
            output.particleCustomData2 = input.particleCustomData2;
        #endif

        return output;
    }

    float SDF_Sphere(float3 p, float s)
    {
        return length(p)-s;
    }

    float SDF_Box(float3 p, float3 b)
    {
        float3 q = abs(p) - b;
        return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
    }

    float2 Sample3DNoise(float3 position)
    {
        float3 tiling1 = _3DTexture1Tiling.xyz * _3DTexture1Tiling.w;
        float3 offset1 = _3DTexture1Scrolling.xyz * _3DTexture1Scrolling.w * _Time.y;
        float3 tiling2 = _3DTexture2Tiling.xyz * _3DTexture2Tiling.w;
        float3 offset2 = _3DTexture2Scrolling.xyz * _3DTexture2Scrolling.w * _Time.y;

        float3 coords1 = position * tiling1 + offset1;
        float4 tex1 = SAMPLE_TEXTURE3D_LOD(_3DNoiseMap, sampler_3DNoiseMap, coords1, 0);

        float3 coords2 = position * tiling2 + offset2;
        coords2 += (tex1.xyz - 0.5) * _3DTexture2Distortion;
        float4 tex2 = SAMPLE_TEXTURE3D_LOD(_3DNoiseMap, sampler_3DNoiseMap, coords2, 0);

        tex1.a *= _3DTexturesMixParams.x;
        tex2.a *= _3DTexturesMixParams.y;

        #ifdef _NOISEMIXMODE_ADD
            float2 mix = tex1.a + tex2.a;
        #elif _NOISEMIXMODE_MUL
            float2 mix = tex1.a * tex2.a;
        #elif _NOISEMIXMODE_MIN
            float2 mix = min(tex1.a, tex2.a);
        #elif _NOISEMIXMODE_MAX
            float2 mix = max(tex1.a, tex2.a);
        #else
            float2 mix = lerp(tex1.a, tex2.a, _3DTexturesMixParams.z);
        #endif

        mix = remapIn01_float2(_3DTextureSmoothstep.xz, _3DTextureSmoothstep.yw, mix);

        return mix;
    }

    float SampleSignedDistanceField(float3 position)
    {
        #ifdef _LOCALSPACEDISTANCEFIELD
            position = TransformWorldToObject(position);
        #endif

        #ifdef _VOLUMESHAPE_SPHERE
            return SDF_Sphere(position, 0.5);
        #else
            return position.y;
        #endif
    }

    float Map(float3 position, ParticlesDatas particlesDatas, bool skipNoiseIfDistancePositive)
    {
        #ifdef _PARTICLESMODE_ON
            float distance = SampleSignedDistanceField((position - particlesDatas.position) / particlesDatas.scale);
        #else
            float distance = SampleSignedDistanceField(position);
        #endif

        float2 noise = 0.0;

        if(skipNoiseIfDistancePositive == true)
        {
            if(distance < 0.0) // Sample Noise Only if needed, actually improve performances
            {
                noise = Sample3DNoise(position - particlesDatas.noiseOffset);
            }
        }
        else
        {
            noise = Sample3DNoise(position - particlesDatas.noiseOffset);
        }


        distance = distance + noise.x * _MapParams.x;

        return lerp(distance, max(distance, -noise.y * _MapParams.z), _MapParams.y);
    }

    float4 GetAlbedo(float distance, float totalDistance, float startRayDistance)
    {
        float albedoColorGradient = remapIn01_float(_ColorParams.x, _ColorParams.y, distance);
        float4 albedo = lerp(_AlbedoColor1, _AlbedoColor2, albedoColorGradient);

        #if defined(FOG_EXP)
            albedo.a *= exp(-(totalDistance - startRayDistance) * unity_FogParams.x);
        #endif

        albedo.rgb *= albedo.a;

        return albedo;
    }

    float GetLightGradient(float3 position, float distance, float3 lightDirection, ParticlesDatas particlesDatas)
    {
        float3 position2 = position + lightDirection * _ColorParams.w;
        float distance2 = Map(position2, particlesDatas, false);
        float lightGradient = saturate((distance2 - distance) / _ColorParams.w);

        return lightGradient;
    }

    float3 ComputeGradient(float distance, float3 position, ParticlesDatas particlesDatas)
    {
        float3 gradient;
        gradient.x = Map(position + float3(_VolumeNormalDistance, 0.0, 0.0), particlesDatas, true);
        gradient.y = Map(position + float3(0.0, _VolumeNormalDistance, 0.0), particlesDatas, true);
        gradient.z = Map(position + float3(0.0, 0.0, _VolumeNormalDistance), particlesDatas, true);
        gradient -= distance;

        return gradient;
    }

    RayMarchOutput RayMarch(RayMarchInput input)
    {
        float totalDistance = 0.0;
        float3 position = input.origin;
        
        RayMarchOutput output = (RayMarchOutput)0.0;

        for(int i = 0; i < _IterationsCount; i++)
        {
            float distance = Map(position, input.particlesDatas, true);

            float contactSoftness = saturate((input.maxDistance - totalDistance) * _ContactSoftness);
            float density = saturate(-distance * input.stepDistance * contactSoftness * _Density);

            float mix = (1.0 - output.albedo.a) * density;
            
            float4 albedo = GetAlbedo(distance, totalDistance, input.startRayDistance);
            output.albedo += albedo * mix;

            float3 gradient = ComputeGradient(distance, position, input.particlesDatas);
            output.normal += gradient * mix;

            float lightGradient = GetLightGradient(position, distance, input.lightDirection, input.particlesDatas);
            output.lightColor += albedo * input.lightColor * lightGradient * mix;

            if(output.albedo.a > 0.99)
            {
                output.albedo.a = 1.0;
                break;
            }

            totalDistance += input.stepDistance;
            
            if(totalDistance >= input.maxDistance)
                break;  

            position += input.stepDistance * input.rayDirection;    
        }

        output.normal = normalize(output.normal);

        return output;
    }

    float GetSceneDepth(float4 projection)
    {
        float rawDepth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, UnityStereoTransformScreenSpaceTex(projection.xy / projection.w)).r;

        if(unity_OrthoParams.w == 0)
        {
            #ifdef _RAYORIGIN_GEOMETRY
                return LinearEyeDepth(rawDepth, _ZBufferParams) - LinearEyeDepth(projection.z / projection.w, _ZBufferParams);
            #else
                return LinearEyeDepth(rawDepth, _ZBufferParams);
            #endif
        }
        else
        {
            #ifdef _RAYORIGIN_GEOMETRY
                return LinearDepthToEyeDepth(rawDepth) - LinearEyeDepth(projection.z / projection.w, _ZBufferParams);
            #else
                return LinearDepthToEyeDepth(rawDepth);
            #endif
        }
    }

    float GetDitheringValues(float2 positionCS, float stepDistance)
    {
        float2 normalizedScreenSpaceUV = GetNormalizedScreenSpaceUV(positionCS);
        normalizedScreenSpaceUV *= _DitherMap_TexelSize.xy;
        normalizedScreenSpaceUV *= _ScreenParams.xy;

        float multiplier = _DitheringFactor * stepDistance;
        return SAMPLE_TEXTURE2D(_DitherMap, sampler_DitherMap, normalizedScreenSpaceUV).x * multiplier;
    }

    float4 Fragment (Varyings input) : SV_Target
    {    
        _IterationsCount = min(_IterationsCount, 256);
        _Density *= _Dissolve;

        _3DTextureSmoothstep.yw += _3DTextureSmoothstep.xz;
        _ColorParams.y += _ColorParams.x;

        #ifdef _PARTICLESMODE_ON
            _AlbedoColor1 *= input.particleColor;
            _AlbedoColor2 *= input.particleColor;
            _Density *= input.particleColor.a;
        #endif

        float sceneDepth = GetSceneDepth(input.projectedPosition);

        Light mainLight = GetMainLight();

        RayMarchInput rayMarchInput = (RayMarchInput)0.0;
        rayMarchInput.rayDirection = -GetWorldSpaceNormalizeViewDir(input.positionWS);
        rayMarchInput.maxDistance = sceneDepth;
        rayMarchInput.stepDistance = _MaxDistance / _IterationsCount;
        rayMarchInput.startRayDistance = input.positionWS.w;

        float ditheringValue = GetDitheringValues(input.positionCS.xy, rayMarchInput.stepDistance);
        
        #ifdef _RAYORIGIN_GEOMETRY
            rayMarchInput.origin = input.positionWS + rayMarchInput.rayDirection * ditheringValue;
        #else
            rayMarchInput.origin = _WorldSpaceCameraPos + rayMarchInput.rayDirection * ditheringValue;
        #endif

        #ifdef _PARTICLESMODE_ON
            ParticlesDatas particlesDatas;
            particlesDatas.position = input.particlePosition;
            particlesDatas.noiseOffset = input.particlePosition * _ParticleParams.x + input.particleScale.www;
            particlesDatas.scale = input.particleScale.xyz;
            particlesDatas.color = input.particleColor;
            rayMarchInput.particlesDatas = particlesDatas;
        #endif

        #ifdef _LIGHTINGMODE_MAINDIRECTIONAL
            rayMarchInput.lightDirection = mainLight.direction;
            rayMarchInput.lightColor = mainLight.color;
        #elif defined(_PARTICLESMODE_ON) && defined(_LIGHTINGMODE_PARTICLECUSTOMDATA)
            rayMarchInput.lightDirection = normalize(input.particleCustomData1.xyz);
            rayMarchInput.lightColor = input.particleCustomData2.xyz * input.particleCustomData2.a;
        #endif

        RayMarchOutput rayMarchOutput = RayMarch(rayMarchInput);

        float4 color = rayMarchOutput.albedo;

        #if !defined(_AMBIENTMODE_NONE)
            #ifdef _AMBIENTMODE_UP
                float3 ambientNormal = float3(0.0, 1.0, 0.0);
            #elif _AMBIENTMODE_OBJECTNORMAL
                float3 ambientNormal = input.normalWS;
            #else 
                float3 ambientNormal = rayMarchOutput.normal;
            #endif

            float3 ambient = SampleSH(ambientNormal);
            color.rgb *= ambient.rgb;
        #endif

        #if defined(_LIGHTINGMODE_MAINDIRECTIONAL) || defined(_LIGHTINGMODE_PARTICLECUSTOMDATA)
            color.rgb += rayMarchOutput.lightColor;       
        #endif

        return color;
    }

#endif